// console.log("Happy Monday?");

// Fetch keyword
	// syntax:
		// fetch('url', {options});
		// in options - method, body and headers

	// GET post data

	fetch("https://jsonplaceholder.typicode.com/posts")
	.then(response => response.json())
	.then(result => showPosts(result))

	// Show post
		// We are going to create a function that will let us show a post from our API.

	const showPosts = (posts) => {
		let entries = ``;

		posts.forEach((post) => {
			entries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
				<button onclick = "editPost(${post.id})">Edit</button>
				<button onclick = "deletePost(${post.id})">Delete</button>
			</div>
			`
		})

		document.querySelector("#div-post-entries").innerHTML = entries;
	};

	// POST data on our API
	// we need to target the form for creating/adding post
	document.querySelector("#form-add-post").addEventListener("submit", (event) => {

		// to change the auto reload of the submit method
		event.preventDefault();

		// post method
			// if we use the post request, the fetch method will return the newly created document
		fetch("https://jsonplaceholder.typicode.com/posts", {
			method: "POST",
			body: JSON.stringify({
				title: document.querySelector("#txt-title").value,
				body: document.querySelector("#txt-body").value,
				userId: 1
			}),
			headers: {
				"Content-Type": "application/json"
			}
		}).then(response => response.json())
		.then(result => {

			alert("Post is successfully added!")

			document.querySelector("#txt-title").value = null;
			document.querySelector("#txt-body").value = null;
		})
	});

	// Edit post

	const editPost = (id) => {

		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body;
		document.querySelector("#txt-edit-id").value = id;

		// removeAttribute will remove the declared attribute from the element
		document.querySelector("#btn-submit-update").removeAttribute("disabled");
	};

	document.querySelector("#form-edit-post").addEventListener("submit", (event) => {

		event.preventDefault();
		let id = document.querySelector("#txt-edit-id").value;
		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: "PUT",
			body: JSON.stringify({
				title: document.querySelector("#txt-edit-title").value,
				body: document.querySelector("#txt-edit-body").value,
				id: id,
				userId: 1
			}),
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(response => response.json())
		.then(result => {

			alert("The post is successfully updated!")

			document.querySelector("#txt-edit-title").value = null;
			document.querySelector("#txt-edit-body").value = null;
			document.querySelector("#txt-edit-id").value = null;

			document.querySelector("#btn-submit-update").setAttribute("disabled", true);
		})
	});

	// Delete Post

	const deletePost = (id) => {
		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: "DELETE"
		})
		.then(response => response.json())
		.then(result => console.log(result))
		document.querySelector(`#post-${id}`).remove()
	};